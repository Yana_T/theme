<?php

/* themes/templates/page.html.twig */
class __TwigTemplate_1163ddf0bcf7c6b03c7ea8bc4386a2fb2e29388e5ffcf98193e8590e92ca491f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"wripper col-md-12 container-fluid\">
    <div class=\"row\">
          <header>
            <div class=\"row col-md-10\">
                <nav role=\"navigation\" class=\"navigation navbar-inverse\">
                    <button type=\"button\" data-target=\"#navbarCollapse\" data-toggle=\"collapse\" class=\"navbar-toggle\">
                                    <span class=\"sr-only\">Toggle navigation</span>
                                    <span class=\"icon-bar\"></span>
                                    <span class=\"icon-bar\"></span>
                                    <span class=\"icon-bar\"></span>
                    </button>
                        <div id=\"navbarCollapse\" class=\"collapse navbar-collapse\">
                            <div class=\"nav nav-pills top_menu col-lg-5 col-md-8\">
                                ";
        // line 14
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "top_navigation", array()), "html", null, true));
        echo "
                            </div>
                        </div>
                </nav>
            </div>
            <div class=\"row col-md-10\">
                <div class=\"left_block col-md-6\">
                    ";
        // line 21
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "main_article", array()), "html", null, true));
        echo "
                </div>
                <div class=\"right_block col-md-5\">
                    <div class=\"player\">
                        ";
        // line 25
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "main_video", array()), "html", null, true));
        echo "
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class=\"row col-md-10\">
          <section class=\"section_one\">
                    <div class=\"left_block col-md-6\">
                     ";
        // line 34
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
                    </div>
                    <div class=\"right_block col-md-5\">
                        <div class=\"servise col-md-12\">
                            ";
        // line 38
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "servises", array()), "html", null, true));
        echo "
                        </div>
                    </div>
            </div>
        </section>
    </div>
    <div class=\"row col-md-10 section_two\">
        <section>
                    <div class=\"row col-md-12\">
                        ";
        // line 47
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "products", array()), "html", null, true));
        echo "
                    </div>
                    <div class=\"row col-md-12\">
                        <nav>
                            <div class=\"nav nav-pills middle_menu col-md-10\">
                                ";
        // line 52
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "middle_nav", array()), "html", null, true));
        echo "
                            </div>
                        </nav>
                    </div>
                    <div class=\"row col-md-12 prodacts\">
                            ";
        // line 57
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "product_items", array()), "html", null, true));
        echo "
                    </div>
                    <div class=\"row col-md-5 section_three\">
                        ";
        // line 60
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "meet_our_team", array()), "html", null, true));
        echo "
                    </div>
        </section>
    </div>
    <div class=\"row col-md-10 socbutt\">
        <section>
            <div class=\"col-md-3\">
                ";
        // line 67
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "block_big_item", array()), "html", null, true));
        echo "
            </div>
            <div class=\"col-md-9\">
                ";
        // line 70
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "block_with_socmenu", array()), "html", null, true));
        echo "
               <div class=\"social\">
                    ";
        // line 72
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "socmenu", array()), "html", null, true));
        echo "
                </div>
                <div class=\"row col-md-12 groups\">
                 <div class=\"item\">
                    ";
        // line 76
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "socmenu_items", array()), "html", null, true));
        echo "
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
                 <div class=\"item\">
                    ";
        // line 80
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "socmenu_items", array()), "html", null, true));
        echo "
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
                    <div class=\"item\">
                    ";
        // line 84
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "socmenu_items", array()), "html", null, true));
        echo "
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
                    <div class=\"item\">
                    ";
        // line 88
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "socmenu_items", array()), "html", null, true));
        echo "
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
              </div>
            </div>
        </section>
    </div>
        <section class='section_four col-md-12'>
               ";
        // line 96
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "design_tips", array()), "html", null, true));
        echo "
               <div method=\"POST\" action=\"#\" class=\"form col-md-9\" name=\"form\">
               ";
        // line 98
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "form_subscribe", array()), "html", null, true));
        echo "
               </div>
        </section>
        <section class=\"section_five col-md-11 pull-right\">
            <div class=\"row col-md-6\">
                ";
        // line 103
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "get_in_tough", array()), "html", null, true));
        echo "
            </div>
            <div class=\"row col-md-12\">
              <div class=\"form_message col-md-7\">
                  ";
        // line 107
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "feedback_form", array()), "html", null, true));
        echo "
              </div>
              <div class=\"black_contact col-md-5 pull-right\">
                  <div class=\"block_contact col-md-10 pull-right\">
                      ";
        // line 111
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "feedback_contact", array()), "html", null, true));
        echo "
                  </div>
              </div>
            </div>
        </section>
      <footer>
          <div class=\"foot_content col-md-10\">
              <div class=\"copir\">
                  <p>";
        // line 119
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "copir", array()), "html", null, true));
        echo "</p>
              </div>
              <div class=\"icons\">
                  ";
        // line 122
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "icons", array()), "html", null, true));
        echo "
              </div>
          </div>
      </footer>





</div>

";
    }

    public function getTemplateName()
    {
        return "themes/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 122,  226 => 119,  215 => 111,  208 => 107,  201 => 103,  193 => 98,  188 => 96,  177 => 88,  170 => 84,  163 => 80,  156 => 76,  149 => 72,  144 => 70,  138 => 67,  128 => 60,  122 => 57,  114 => 52,  106 => 47,  94 => 38,  87 => 34,  75 => 25,  68 => 21,  58 => 14,  43 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"wripper col-md-12 container-fluid\">
    <div class=\"row\">
          <header>
            <div class=\"row col-md-10\">
                <nav role=\"navigation\" class=\"navigation navbar-inverse\">
                    <button type=\"button\" data-target=\"#navbarCollapse\" data-toggle=\"collapse\" class=\"navbar-toggle\">
                                    <span class=\"sr-only\">Toggle navigation</span>
                                    <span class=\"icon-bar\"></span>
                                    <span class=\"icon-bar\"></span>
                                    <span class=\"icon-bar\"></span>
                    </button>
                        <div id=\"navbarCollapse\" class=\"collapse navbar-collapse\">
                            <div class=\"nav nav-pills top_menu col-lg-5 col-md-8\">
                                {{ page.top_navigation }}
                            </div>
                        </div>
                </nav>
            </div>
            <div class=\"row col-md-10\">
                <div class=\"left_block col-md-6\">
                    {{page.main_article}}
                </div>
                <div class=\"right_block col-md-5\">
                    <div class=\"player\">
                        {{page.main_video}}
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class=\"row col-md-10\">
          <section class=\"section_one\">
                    <div class=\"left_block col-md-6\">
                     {{page.content}}
                    </div>
                    <div class=\"right_block col-md-5\">
                        <div class=\"servise col-md-12\">
                            {{page.servises}}
                        </div>
                    </div>
            </div>
        </section>
    </div>
    <div class=\"row col-md-10 section_two\">
        <section>
                    <div class=\"row col-md-12\">
                        {{page.products}}
                    </div>
                    <div class=\"row col-md-12\">
                        <nav>
                            <div class=\"nav nav-pills middle_menu col-md-10\">
                                {{page.middle_nav}}
                            </div>
                        </nav>
                    </div>
                    <div class=\"row col-md-12 prodacts\">
                            {{page.product_items}}
                    </div>
                    <div class=\"row col-md-5 section_three\">
                        {{page.meet_our_team}}
                    </div>
        </section>
    </div>
    <div class=\"row col-md-10 socbutt\">
        <section>
            <div class=\"col-md-3\">
                {{page.block_big_item}}
            </div>
            <div class=\"col-md-9\">
                {{page.block_with_socmenu}}
               <div class=\"social\">
                    {{page.socmenu}}
                </div>
                <div class=\"row col-md-12 groups\">
                 <div class=\"item\">
                    {{page.socmenu_items}}
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
                 <div class=\"item\">
                    {{page.socmenu_items}}
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
                    <div class=\"item\">
                    {{page.socmenu_items}}
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
                    <div class=\"item\">
                    {{page.socmenu_items}}
                    <div class=\"hidden_block\">sokina jue</div>
                 </div>
              </div>
            </div>
        </section>
    </div>
        <section class='section_four col-md-12'>
               {{page.design_tips}}
               <div method=\"POST\" action=\"#\" class=\"form col-md-9\" name=\"form\">
               {{page.form_subscribe}}
               </div>
        </section>
        <section class=\"section_five col-md-11 pull-right\">
            <div class=\"row col-md-6\">
                {{page.get_in_tough}}
            </div>
            <div class=\"row col-md-12\">
              <div class=\"form_message col-md-7\">
                  {{page.feedback_form}}
              </div>
              <div class=\"black_contact col-md-5 pull-right\">
                  <div class=\"block_contact col-md-10 pull-right\">
                      {{page.feedback_contact}}
                  </div>
              </div>
            </div>
        </section>
      <footer>
          <div class=\"foot_content col-md-10\">
              <div class=\"copir\">
                  <p>{{page.copir}}</p>
              </div>
              <div class=\"icons\">
                  {{page.icons}}
              </div>
          </div>
      </footer>





</div>

";
    }
}
